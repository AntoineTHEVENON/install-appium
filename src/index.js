// javascript

// déclaration des constantes
const wdio = require("webdriverio");																	// éditable : importation des libraries requisent
const assert = require("assert");

const opts = {

	// http://localhost:4723/wd/hub
	path: '/wd/hub',
	port: 4723,

	capabilities: {

		// utilisation d'Android 8
		platformName: "Android",																		// éditable : OS choisi dans Android Studio
		// platformVersion: "8",																		// éditable : version de l'OS choisi

		// nom du device créé dans l'emulateur sous Android Studio
		deviceName: "Android Emulator",																	// éditable : nom de machine virtuelle dans Android Studio

		// appli .apk en chemin relatif (côté Windows)
		app: __dirname + "/../apk/ApiDemos-debug.apk",													// éditable : chemin de l'apk du tutoriel (côté Windows)

		// package et activité concerné par ce test (côté Android)
		appPackage: "io.appium.android.apis",															// éditable : nom du package de l'application du tutoriel à tester (côté Machine Virtuelle)
		appActivity: ".view.TextFields",																// éditable : activité du package à tester (côté Machine Virtuelle)

		// diver (automate) à utiliser pour le test
		automationName: "UiAutomator2"																	// éditable : driver (automate) du tutoriel pour Android

	}

};

// déclaration du main en tant que fonction asynchrone
async function main () {

	// création d'une session d'utilisateur virtuel
	const client = await wdio.remote(opts);

	// saisie de la chaîne "Hello World!" dans le widget EditText
	const field = await client.$("android.widget.EditText");											// éditable : indique une saisie de texte dans le champ actif (champ par défaut) à la machine virtuelle
	await field.setValue("Hello World!");																// éditable : données saisie

	// contrôle de correspondance entre la chaîne présente dans le widget EditText et le résutat prévu
	const value = await field.getText();																// éditable : récupération des données dans le champ actif
	assert.strictEqual(value,"Hello World!");															// éditable : vérification de la corrélation entre les données saisies et la valeur du champ actif

	// destruction de la session d'utilisateur virtuel
	await client.deleteSession()

};

// appel du main
main();
