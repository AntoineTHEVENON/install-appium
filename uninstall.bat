@echo off
chcp 65001
goto checkpermissions

:checkpermissions
	net session >nul 2>&1
	if %errorLevel% == 0 (
		if "%1" == "done" goto runtime
		start "" /max %0 done
		exit
	) else (
		powershell "start '%0' -v runAs"
		exit
	)

:runtime
	SetLocal EnableDelayedExpansion
	call cd "%ANDROID_HOME%\platform-tools"
	call adb kill-server
	call md C:\TempAppiumUninstall
	call cd C:\TempAppiumUninstall
	call npm uninst -g appium-flutter-finder
	call npm uninst -g appium-flutter-driver
	call npm uninst -g appium-doctor
	call npm uninst -g appium
	call npm uninst -g webdriverio
	call choco uninstall nodejs.install -y
	call choco uninstall appium-desktop -y
	call choco uninstall androidstudio -y
	call reg query "HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Session Manager\Environment" /v PATH | findstr /v HKEY_LOCAL_MACHINE > temp
	for /f "delims=" %%x in (temp) do set x=%%x
	call set x=%x:    PATH    REG_EXPAND_SZ    =%
	call set x=%x:    PATH    REG_SZ    =%
	call set x=!x:%ANDROID_HOME%;=!
	call set x=!x:%ANDROID_HOME%\tools;=!
	call set x=!x:%ANDROID_HOME%\tools\bin;=!
	call set x=!x:%ANDROID_HOME%\platform-tools;=!
	call set x=!x:%PROGRAMFILES%\Android\Android Studio\jre\bin;=!
	call set x=!x:%USERPROFILE%\AppData\Roaming\npm\;=!
	call set x=!x:%USERPROFILE%\AppData\Roaming\npm\node_modules\webdriverio;=!
	call set x=%x%;eol;
	call set x=%x:;;eol;=%
	call set x=%x:;eol;=%
	call reg add "HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Session Manager\Environment" /v PATH /t REG_SZ /d "%x%;" /f
	call refreshenv
	call rd /s /q "%USERPROFILE%\AppData\Roaming\appium-desktop"
	call rd /s /q "%USERPROFILE%\AppData\Local\appium-desktop-updater"
	call rd /s /q "%USERPROFILE%\AppData\Roaming\npm"
	call rd /s /q "%USERPROFILE%\AppData\Local\npm-cache"
	call rd /s /q "%USERPROFILE%\AppData\Local\Android Open Source Project"
	call rd /s /q "%USERPROFILE%\AppData\Local\Android"
	call rd /s /q "%PROGRAMFILES%\Android"
	call rd /s /q "%USERPROFILE%\.android"
	call cd..
	call rd /s /q C:\TempAppiumUninstall
	pause
	exit