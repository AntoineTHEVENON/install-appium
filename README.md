# install-appium

## I - Récupération des données

Télécharger le git au format  [**zip**](https://gitlab.com/AntoineTHEVENON/install-appium/-/archive/main/install-appium-main.zip) et extraire les fichiers et dossiers de l'archive

![01-ddl-git.bmp](https://gitlab.com/AntoineTHEVENON/install-appium/-/raw/main/pictures/01-ddl-git.bmp)

## II - Installation d'*Appium*

Installer automatiquement l'ensemble des outils via le script **install.bat** et attendre la fin de l'installation, indiquée par la ligne **Appuyez sur une touche pour continuer...** (ignorer les deux erreurs de *sdk* manquant et les messages de warning d'*Appium Doctor*) :

- installe [*Chocolatey*](https://chocolatey.org/)

- installe [*Java 8 SE*](https://www.java.com/fr/download/help/java8_fr.html) (JRE et JDK)

- installe [*Node.js*](https://nodejs.org/fr/)

- installe [*Android Studio*](https://developer.android.com/studio)

- installe [*Appium*](https://appium.io/) (desktop & command line)

- installe [*Appium Doctor*](https://github.com/appium/appium-doctor)

- installe [*Appium Flutter Driver* & *Appium Flutter Finder*](https://github.com/appium-userland/appium-flutter-driver)

- installe [*WebDriver*](https://webdriver.io/)

- complémente les chemins nécessaires à la variable d'environnement **%PATH%** et ajoute celles de **%JAVA_HOME%** et **%ANDROID_HOME%**

![02-install.bmp](https://gitlab.com/AntoineTHEVENON/install-appium/-/raw/main/pictures/02-install.bmp)

Lancer *Android Studio* en premier, sans changer les paramètres prédéfinis et en téléchargeant le *SDK* dans l'emplacement par défaut (**C:\Users\ <nom_d_utilisateur> \AppData\Local\Android\Sdk**), puis accéder à l'*ADV Manager*

![03-configure-sdk.bmp](https://gitlab.com/AntoineTHEVENON/install-appium/-/raw/main/pictures/03-configure-sdk.bmp)

Choisir le type et le modèle du device à émuler et télécharger l'api *Android* désirée (**ATTENTION** : les devices ne portant pas l'icone du *Google Play* n'en disposeront pas sans une manipulation préalable au premier lancement de l'émulateur !)

![04-configure-device.bmp](https://gitlab.com/AntoineTHEVENON/install-appium/-/raw/main/pictures/04-configure-device.bmp)

## III - Installer le *Google Play* (sur les devices non prévus pour) et éditer l'attribution de mémoire

Accéder au répertoire de l'émulateur via **Show on Disk** et ouvrir le fichier **config.ini**

![05-open-config.bmp](https://gitlab.com/AntoineTHEVENON/install-appium/-/raw/main/pictures/05-open-config.bmp)

Editer les lignes **PlayStore.enable** à **true** et **image.sysdir.1** avec **google_apis_playstore** pour activer le *Google Play* et les lignes **hw.cpu.ncore** et **hw.ramSize** pour gérer le nombre de coeurs et la mémoire vive disponibles sur le device (ne pas dépasser la moitié des coeurs et de la mémoire vive du système hôte)

![06-edit-device.bmp](https://gitlab.com/AntoineTHEVENON/install-appium/-/raw/main/pictures/06-edit-device.bmp)

Accéder au répertoire d'*Android Studio* sous **C:\Program Files\Android\Android Studio\bin** et éditer les lignes **-Xms**, **-Xmx** et **-XX** pour gérer la mémoire vive réservée à l'émulateur à son lancement, utilisable au plus par celui-ci et réservée au cache du code

![07-edit-android-studio.bmp](https://gitlab.com/AntoineTHEVENON/install-appium/-/raw/main/pictures/07-edit-android-studio.bmp)

Vérifier la bonne activation du *Google Play* par la présence de son icone sur le device édité, après avoir actualisé l'*ADV Manager*

![08-confirm-google-play.bmp](https://gitlab.com/AntoineTHEVENON/install-appium/-/raw/main/pictures/08-confirm-google-play.bmp)

## IV - Vérifier le bon fonctionnement d'*Android Studio*, d'*Appium* et de leur mise en relation en effectuant le script du tutoriel d'*Appium*

Lancer l'émulateur créé précédement et autoriser l'accès au réseau pour *Android Studio*

![09-adv-launch.bmp](https://gitlab.com/AntoineTHEVENON/install-appium/-/raw/main/pictures/09-adv-launch.bmp)

Vérifier la bonne présence des scripts **launch.bat**, **launch-appium-desktop.bat** et des dossiers **apk** (contenant l'application mobile **ApiDemos-debug.apk**) et **src** (contenant le script **index.js**) au même emplacement

![10-project-folder.bmp](https://gitlab.com/AntoineTHEVENON/install-appium/-/raw/main/pictures/10-project-folder.bmp)

Lancer le script du tutoriel d'*Appium* via le script **launch.bat** et saisir **index.js** en tant que **nom du script**, puis attendre la fin du test, indiquée par la ligne **Appuyez sur une touche pour continuer...**, avant de fermer la fenêtre du script et celle d'*Appium*

![11-appium-tutorial.bmp](https://gitlab.com/AntoineTHEVENON/install-appium/-/raw/main/pictures/11-appium-tutorial.bmp)

(**optionnel**) Désinstaller l'application du tutoriel d'*Appium* (*API Demos*) du device pour contrôler le bon fonctionnement du service *adb* sur *Appium Desktop*

![12-apk-uninstall.bmp](https://gitlab.com/AntoineTHEVENON/install-appium/-/raw/main/pictures/12-apk-uninstall.bmp)

Lancer *Appium Desktop* depuis son raccourci, cliquer sur le bouton **Start Server <numero_de_version>**, puis lancer le script du tutoriel d'*Appium Desktop* via le script **launch-appium-desktop.bat**, en saisissant **index.js** en tant que **nom du script** et attendre la fin du test, indiquée par la ligne **Appuyez sur une touche pour continuer...**

![13-appium-desktop-tutorial.bmp](https://gitlab.com/AntoineTHEVENON/install-appium/-/raw/main/pictures/13-appium-desktop-tutorial.bmp)

## V - Vérifier le bon fonctionnement des visionneuses *XML*

Cliquer sur **Start Inspector Session** (icone de loupe) ou **New Session Window... Ctrl+N** du menu **File** dans *Appium* pour accéder à sa visionneuse **XML**

![14-appium-viewer-acces.bmp](https://gitlab.com/AntoineTHEVENON/install-appium/-/raw/main/pictures/14-appium-viewer-acces.bmp)

Saisir **platformName** dans le champ **Name** et **Android** dans le champ **Value**, puis cliquer sur **Start Session** pour lancer la visionneuse **XML**

![15-appium-viewer.bmp](https://gitlab.com/AntoineTHEVENON/install-appium/-/raw/main/pictures/15-appium-viewer.bmp)

Fermer **impérativement** *Appium* et sa visionneuse **XML** pour pouvoir tester celle d'*UiAutomator*

Lancer l'**uiautomatorviewer** via le script **ui-automator-viewer.bat**, puis cliquer sur **Device Screenshot (uiautomator dump)**

![16-uiautomatorviewer.bmp](https://gitlab.com/AntoineTHEVENON/install-appium/-/raw/main/pictures/16-uiautomatorviewer.bmp)

## VI - Désinstaller les outils

Désinstaller automatiquement les outils via le script **uninstall.bat** et attendre la fin de la désinstallation, indiquée par la ligne **Appuyez sur une touche pour continuer...** :

- désinstalle [*WebDriver*](https://webdriver.io/)

- désinstalle [*Appium Flutter Driver* & *Appium Flutter Finder*](https://github.com/appium-userland/appium-flutter-driver)

- désinstalle [*Appium Doctor*](https://github.com/appium/appium-doctor)

- désinstalle [*Appium*](https://appium.io/) (desktop & command line)

- désinstalle [*Android Studio*](https://developer.android.com/studio)

- désinstalle [*Node.js*](https://nodejs.org/fr/)

- supprime les chemins inutiles à la variable d'environnement **%PATH%**

![17-uninstall.bmp](https://gitlab.com/AntoineTHEVENON/install-appium/-/raw/main/pictures/17-uninstall.bmp)