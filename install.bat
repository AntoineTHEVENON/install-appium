@echo off
chcp 65001
goto checkpermissions

:checkpermissions
	net session >nul 2>&1
	if %errorLevel% == 0 (
		if "%1" == "done" goto runtime
		start "" /max %0 done
		exit
	) else (
		powershell "start '%0' -v runAs"
		exit
	)

:runtime
	call md C:\TempAppiumInstall
	call cd C:\TempAppiumInstall
	@"%SYSTEMROOT%\System32\WindowsPowerShell\v1.0\powershell.exe" -NoProfile -ExecutionPolicy Bypass -Command "iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))"
	call %ALLUSERSPROFILE%\chocolatey\bin\refreshenv.cmd
	call choco install jre8 -y --force
	call choco install jdk8 -y --force
	call refreshenv
	call choco install androidstudio -y --params "/AddToDesktop"
	call reg add "HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Session Manager\Environment" /v ANDROID_HOME /t REG_SZ /d "%USERPROFILE%\AppData\Local\Android\Sdk" /f
	call refreshenv
	call choco install appium-desktop -y
	call choco install nodejs.install -y
	call refreshenv
	call reg query "HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Session Manager\Environment" /v PATH | findstr /v HKEY_LOCAL_MACHINE > temp
	for /f "delims=" %%x in (temp) do set x=%%x
	call set x=%x:    PATH    REG_EXPAND_SZ    =%
	call set x=%x:    PATH    REG_SZ    =%
	call reg add "HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Session Manager\Environment" /v PATH /t REG_SZ /d "%x%;%ANDROID_HOME%;%ANDROID_HOME%\tools;%ANDROID_HOME%\tools\bin;%ANDROID_HOME%\platform-tools;%PROGRAMFILES%\Android\Android Studio\jre\bin;%USERPROFILE%\AppData\Roaming\npm\;%USERPROFILE%\AppData\Roaming\npm\node_modules\webdriverio;" /f
	call refreshenv
	call npm init -y
	call npm i -g
	call npm i -g webdriverio
	call npm i -g appium
	call npm i -g appium-doctor
	call npm i -g appium-flutter-driver
	call npm i -g appium-flutter-finder
	call refreshenv
	call cd..
	call rd /s /q C:\TempAppiumInstall
	call appium-doctor
	pause
	exit