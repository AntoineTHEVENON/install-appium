@echo off
chcp 65001
goto checkpermissions

:checkpermissions
	net session >nul 2>&1
	if %errorLevel% == 0 (
		if "%1" == "done" goto runtime
		call cd "%~dp0src"
		start /max appium -a 127.0.0.1 -p 4723
		call timeout /t 5 /nobreak >null
		call del /s null
		start "" /max %0 done
		exit
	) else (
		powershell "start '%0' -v runAs"
		exit
	)

:runtime
	call cd "%%~dp0src"
	call set /p file=nom du script .js : 
	call npm link webdriverio
	call appium-doctor
	call node "%file%"
	pause
	exit